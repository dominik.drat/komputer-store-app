# Komputer Store App

Project for simple website, using HTML, css and JavaScript. It also uses sample API for getting computers data.

This simple web app is an example interface for a computer store. User is able to:
- increase his/her pay balance
- transfer his earnings to bank balance
- obtain a loan
- select a computer from the list
- buy the computer if bank balance is sufficient

There are some restrictions, for example:
- loan cannot be negative, zero, and higher than double the amount of current bank balance
- having a loan means, that money transferred form pay balance in usual way will be split: 90% to will go to bank balance, 10% for outstanding loan
- only one loan can be active at the time

Pipeline link: https://dominik.drat.gitlab.io/komputer-store-app/
