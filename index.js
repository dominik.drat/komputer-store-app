// Document elements which will be used
const repayLoanElement = document.getElementById("repayLoan")
const loanTextElement = document.getElementById("loanText")
const outstandingLoanElement = document.getElementById("outstandingLoan")
const balanceElement = document.getElementById("balance");
const getLoanElement = document.getElementById("getLoan");
const payButtonElement = document.getElementById("pay");
const bankButtonElement = document.getElementById("bank");
const workButtonElement = document.getElementById("work");
const laptopsElement = document.getElementById("laptops");
const specsElement = document.getElementById("specs");
const buyButtonElement = document.getElementById("buy");
const computerImageElement = document.getElementById("computerImage");
const descriptionElement = document.getElementById("description");
const priceElement = document.getElementById("price");
const computerNameElement = document.getElementById("computerName");

// Variables
let laptops = [];
const apiString = 'https://noroff-komputer-store-api.herokuapp.com/'
const errorImage = 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/300px-No_image_available.svg.png'
let bankBalance = 0.0;
let outstandingLoan = 0.0;
let payBalance = 0.0;
let activeLoan = false;
let currentLaptopPrice = 0;

// Default values, setting loan related elements to act as if loan is not active
putDefaultValues(bankBalance, outstandingLoan, payBalance);
loanIsNotActive();

// Fetching API for laptops data
fetch(apiString + "computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops))

// Adding all laptops to dropdown list
const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addSingleLaptopToMenu(x));

    // Website starts with the first laptop as selected
    specsElement.innerHTML = makeSpecs(laptops[0].specs);
    priceElement.innerHTML = laptops[0].price + " DKK";
    currentLaptopPrice = laptops[0].price;
    descriptionElement.innerHTML = laptops[0].description;
    computerNameElement.innerHTML = laptops[0].title;
    computerImageElement.innerHTML = `<img src="${apiString + laptops[0].image}" width="250px" alt="Could not load the image">`;
}

// Adding single laptop
const addSingleLaptopToMenu = (laptop) => {
    const singleLaptopElement = document.createElement("option");
    singleLaptopElement.value = laptop.id;
    singleLaptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(singleLaptopElement);
}

// Function handling UI changes on changing selection of laptop in the dropdown
const handleLaptopMenuChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    specsElement.innerHTML = makeSpecs(selectedLaptop.specs);
    priceElement.innerHTML = selectedLaptop.price + " DKK";
    currentLaptopPrice = selectedLaptop.price;
    descriptionElement.innerHTML = selectedLaptop.description;
    computerNameElement.innerHTML = selectedLaptop.title;
    changeComputerImage(selectedLaptop.image);
}

// Function that changes default display of "specs" element
function makeSpecs(specs) {
    let str = specs.map(function (item) {
        return item;
    }).join("</br>");
    return str;
}

// Changing image to match the one of selected computer
function changeComputerImage(image) {
    computerImageElement.innerHTML = `<img src="${apiString + image}" onerror=this.src=errorImage width="250px" alt="Could not load the image">`;
}

// Default values function; used only once, puts everything to zero
function putDefaultValues() {
    assignBankBalance();
    assignPayBalance();
    outstandingLoanElement.innerHTML = 0 + ' DKK';
}

// Function for increasing pay balance by 100
function increasePay() {
    payBalance += 100;
    assignPayBalance();
}

// Function for transferring money from pay balance to bank balance with condition for loan
function transferToBank() {
    if (activeLoan === true) {
        bankBalance += payBalance * 0.9;
        outstandingLoan -= payBalance * 0.1;
        assignOutstandingLoan();
    } else {
        bankBalance += payBalance;
    }
    payBalance = 0;
    assignPayBalance();
    assignBankBalance();
}

// Function for repaying loan
function repayLoan() {
    outstandingLoan -= payBalance;
    payBalance = 0;
    assignPayBalance();
    assignOutstandingLoan();
}

// Function for assigning bank balance
function assignBankBalance() {
    balanceElement.innerHTML = bankBalance + ' DKK';
}

// Function for assigning pay balance
function assignPayBalance() {
    payButtonElement.innerHTML = payBalance + ' DKK';
}

// Function for assigning loan balance
function assignOutstandingLoan() {
    outstandingLoanElement.innerHTML = outstandingLoan + ' DKK';
    if (outstandingLoan <= 0 && activeLoan) {
        bankBalance -= outstandingLoan;
        assignBankBalance();
        loanIsNotActive();
        alert('Loan has been repaid.\nExcess money from loan balance were transferred to bank.');
    }
}

// Function for changing UI if loan is active
function loanIsActive() {
    outstandingLoanElement.hidden = false;
    loanTextElement.hidden = false;
    repayLoanElement.hidden = false;
    activeLoan = true;
}

// Function for changing UI if loan is not active
function loanIsNotActive() {
    outstandingLoanElement.hidden = true;
    loanTextElement.hidden = true;
    repayLoanElement.hidden = true;
    activeLoan = false;
}

// Function for obtaining loan
const handleLoan = () => {
    const loanMoney = prompt("Please enter the amount of money you wish to loan: ");
    const loan = parseFloat(loanMoney);
    if (loan > 0 && loan <= (bankBalance * 2) && activeLoan === false) {
        outstandingLoan = loan;
        bankBalance += loan;
        loanIsActive();
        assignOutstandingLoan();
        assignBankBalance();
        alert(`Loan accepted`);
    } else {
        alert(`Could not accept the loan.\n\nOnly one loan can be active.\nLoan cannot be greater than double the amount of money in bank.`);
    }
}

// Function for buying a computer
function buyLaptop() {
    if (currentLaptopPrice > bankBalance) {
        alert('You cannot afford to buy this computer at the moment.')
    } else {
        bankBalance -= currentLaptopPrice;
        assignBankBalance();
        alert('Congratulations! You are now the owner of the new computer!');
    }
}

// Event listeners for buttons
laptopsElement.addEventListener("change", handleLaptopMenuChange);
workButtonElement.addEventListener("click", increasePay);
bankButtonElement.addEventListener("click", transferToBank);
getLoanElement.addEventListener("click", handleLoan);
repayLoanElement.addEventListener("click", repayLoan);
buyButtonElement.addEventListener("click", buyLaptop);